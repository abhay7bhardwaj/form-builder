export enum ConfigName {
  apollo = "exai-layout-apollo",
  zeus = "exai-layout-zeus",
  hermes = "exai-layout-hermes",
  poseidon = "exai-layout-poseidon",
  ares = "exai-layout-ares",
  ikaros = "exai-layout-ikaros",
}
