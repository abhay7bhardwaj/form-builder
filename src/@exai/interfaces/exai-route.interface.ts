import { Route } from '@angular/router';

export interface ExRouteData {
  scrollDisabled?: boolean;
  toolbarShadowEnabled?: boolean;
  containerEnabled?: boolean;

  [key: string]: any;
}

export interface ExRoute extends Route {
  data?: ExRouteData;
  children?: ExRoute[];
}

export type ExRoutes = ExRoute[];
