import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomLayoutModule } from './custom-layout/custom-layout.module';
import { ExaiDynamicFormsModule } from './exai-dynamic-forms/exai-dynamic-forms.module';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomLayoutModule,
    ExaiDynamicFormsModule,
  ],
  providers: [
],
  bootstrap: [AppComponent],
})
export class AppModule {}


