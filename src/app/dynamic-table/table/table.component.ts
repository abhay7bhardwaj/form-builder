import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import icMoreHoriz from "@iconify/icons-ic/twotone-more-horiz";
import icCloudDownload from "@iconify/icons-ic/twotone-cloud-download";
import { Router, ActivatedRoute } from "@angular/router";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { SelectionModel } from "@angular/cdk/collections";
import {
  tableOptions,
  status,
  action,
  column,
  performedAction,
  cellClickEvent,
  headerClickEvent,
  headerDropdownSelectionEvent,
} from "../dynamic-table.types";
import { FormControl } from "@angular/forms";
import { MatSort } from "@angular/material/sort";
@Component({
  selector: "exai-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"],
})
export class TableComponent implements OnInit, AfterViewInit {
  icMoreHoriz = icMoreHoriz;
  icCloudDownload = icCloudDownload;

  // pageSize = 10;
  @ViewChild("matPaginator")paginator: MatPaginator; 
  @Input() tableOptions: tableOptions;
  @Input() dataSource: MatTableDataSource<any>;
  @Input() displayedColumns: Array<any>;
  @ViewChild(MatSort) sort: MatSort;

  @Output("actionPerformed") action: EventEmitter<performedAction> =
    new EventEmitter<performedAction>();
  @Output("cellClickEvent") cellClicked: EventEmitter<cellClickEvent> =
    new EventEmitter<cellClickEvent>();
  @Output("headerClicked") headerClicked: EventEmitter<headerClickEvent> =
    new EventEmitter<headerClickEvent>();
  @Output("headerDropdownSelection")
  headerDropdownSelection: EventEmitter<headerDropdownSelectionEvent> = new EventEmitter<headerDropdownSelectionEvent>();

  displayedColumnsIndex: Array<string>;
  searchControl: FormControl;
  selection = new SelectionModel<''>(true, []);

  constructor(
    private activateRotute: ActivatedRoute, 
    private router: Router) {
    this.searchControl = new FormControl("");
  }

  ngOnInit(): void {
    // intitialising output evenemitters

    this.searchControl.valueChanges.subscribe((value: string) => {
      this.dataSource.filter = value.trim().toLocaleLowerCase();
    });
    this.displayedColumnsIndex = this.displayedColumns.map((x) => {
      return x.id;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filterThings(things: Array<any>, searchTerm: string) {
    return things.filter((item: any) => {
      return JSON.stringify(item)
        .toLowerCase()
        .includes(searchTerm.toLowerCase());
    });
  }

  actionHandler(
    column: column,
    element: any,
    rowIndex: number,
    actionIndex: number
  ) {
    this.action.emit(<performedAction>{
      column,
      element,
      rowIndex,
      action: column.actions[actionIndex],
    });
  }
  cellClickHandler(column: column, element: any, rowIndex) {
    this.cellClicked.emit(<cellClickEvent>{
      column,
      element,
      rowIndex,
    });
  }
  headerClickHandler(column: column) {
    this.headerClicked.emit(<headerClickEvent>{
      column,
    });
  }
  headerDropdownSelectionHandler(column: column, dropdownKey: any) {
    this.headerDropdownSelection.emit(<headerDropdownSelectionEvent>{
      column,
      dropdownKey,
    });
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
}


