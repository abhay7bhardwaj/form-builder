export interface column {
  id: string;
  name: string;
  cellType?: "action" | "status" | "text" | "number";
  actions?: any;
  statuses?: any;
  headerType?: "normal" | "dropdown" | "sort"| "checkbox";
  dropDownActions?: Array<string>;
  // these string identifiers will be present in the dropdowns and the event emitter will contain a key with this value to process it
  headerAlign?: "center" | "right" | "left";
  cellAlign?: "center" | "right" | "left";
  headerCssClasses?: Array<string>;
  cellCssClasses?: Array<string>;
}

export interface tableOptions {
  tableName?: string;
  minWidth: string;
  minHeight: string;
  maxWidth: string;
  maxHeight: string;
  showPaginator: boolean;
  pageSizeOptions: Array<number>;
  pageSize?: number;
  showSearch: boolean;
  horizontalScroll?: boolean;
  verticalScroll?: boolean;
}

export interface status {
  icon: string;
  color: string;
  text: string;
}
export interface action {
  icon: string;
  color: string;
  tooltip: string;
}

export interface RegistrationReport {
  index: string;
  cid: string;
  cname: string;
  date: string;
  time: string;
  zone: string;
  status: string;
  view: Array<string>;
}

export class performedAction {
  action: action;
  column: column;
  element: any;
  rowIndex: number;
}

export class cellClickEvent {
  column: column;
  element: any;
  rowIndex: number;
}
export class headerClickEvent {
  column: column;
}
export class headerDropdownSelectionEvent {
  column: column;
  dropdownKey: string;
}
