import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "formbuilder",
    loadChildren: () =>
      import("./examroom-formbuilder/form-builder.module").then(
        (m) => m.FormBuilderModule
      ),
  },
  {
    path: "dynTable",
    loadChildren: () =>
      import("./dynamic-table/dynamic-table.module").then(
        (m) => m.DynamicTableModule
      ),
  },
  { path: "**", pathMatch: "full", redirectTo: "formbuilder" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
